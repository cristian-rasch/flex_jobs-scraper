# FlexJobs::Scraper

flexjobs.com job scraper library

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'flex_jobs-scraper', git: 'git@bitbucket.org:cristian-rasch/flex_jobs-scraper.git', require: 'flex_jobs/scraper'
```

And then execute:

    $ bundle

## Usage

```ruby
require "flex_jobs/scraper"
require "pp"

# List job categories
FlexJobs::Search.list_categories # => ["Account Management", "Client Services", "Accounting & Finance", ..]

# List career levels
FlexJobs::Search.career_levels # => ["Entry-Level", "Experienced (non-Manager)", "Manager (Supervisor role)", "Executive"]

# List work schedules
FlexJobs::Search.work_schedules # => ["Full-Time", "Part-Time", "Flexible Schedule", "Alternative Schedule", "Occasional", "Seasonal", "Long-Term", "Short-Term"]

# List job types
FlexJobs::Search.job_types # => ["Temporary", "Freelance"]

# Job search
flex_jobs = FlexJobs::Scraper.new
# Options include:
#   - categories - restrict the search to the specified job categories as listed by the FlexJobs::Search.list_categories method.
#   - career_level - restrict the search to the specified experience level as listed by the FlexJobs::Search.career_levels method.
#   - work_schedule - restrict the search to the specified work schedule as listed by the FlexJobs::Search.work_schedules method.
#   - job_types - restrict the search to the specified job types as listed by the FlexJobs::Search.job_types method.
#                 Defaults to temporary and freelance jobs.
#   - limit - how many jobs to retrieve (defaults to 100, max. 100)
#   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
#   - logger - a ::Logger instance to log error messages to
jobs = flex_jobs.job_search("web designer", categories: ["Web Design"], limit: 1)

# #<struct FlexJobs::Scraper::Job
#  title="Web Designer",
#  id="328615",
#  url="http://www.flexjobs.com/publicjobs/web-designer-328615",
#  description=
#   "Seeking freelance remote web developer for a short-term position slated to last three to four weeks. Candidate must have four years of experience in a similar role and industry working knowledge. Previous work samples needed for consideration.",
#  created_on=#<Date: 2015-02-19 ((2457073j,0s,0n),+0s,2299161j)>,
#  categories=["Web Design", "Fashion & Beauty"],
#  location=["Toronto", "ON", "Canada "],
#  type=["Freelance", "Temporary", "Short-Term"],
#  hours_per_week=nil,
#  career_level="Experienced (non-Manager)",
#  flexibility=["Telecommuting", "Freelancing Job"],
#  salary=nil,
#  benefits=["Short-Term Schedule"]>
```
