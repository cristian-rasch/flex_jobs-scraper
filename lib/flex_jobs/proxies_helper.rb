require "dotenv"
Dotenv.load
require "open-uri"
require "logger"

module FlexJobs
  module ProxiesHelper
    USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0".freeze

    def open_through_proxy(url)
      begin
        open(url, "User-Agent" => USER_AGENT, "proxy" => next_proxy)
      rescue => err
        logger.error "#{err.message} opening '#{url}'"
        nil
      end
    end

    private

    def next_proxy
      proxies.shift.tap { |proxy| proxies << proxy }
    end

    def proxies
      @proxies ||= ENV.fetch("FLEX_JOBS_PROXIES").split(",").shuffle
    end
  end
end
