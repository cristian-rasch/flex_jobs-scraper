require_relative "scraper/version"
require_relative "search"
require "set"

module FlexJobs
  class Scraper
    BASE_URL = "http://www.flexjobs.com/".freeze
    SEARCH_PATH = "/search".freeze
    DEFAULT_LIMIT = 100
    MAX_PAGE = 25
    DATE_FORMAT = "%m/%d/%y".freeze
    SEPARATED_BY_COMMAS_REGEXP = /,\s+/

    class Job < Struct.new(:title, :id, :url, :description, :created_on, :categories, :location,
                           :type, :hours_per_week, :career_level, :flexibility, :salary, :benefits)
    end

    include ProxiesHelper

    # Options include:
    #   - categories - restrict the search to the specified job categories as listed by the FlexJobs::Search.list_categories method.
    #   - career_level - restrict the search to the specified experience level as listed by the FlexJobs::Search.career_levels method.
    #   - work_schedule - restrict the search to the specified work schedule as listed by the FlexJobs::Search.work_schedules method.
    #   - job_types - restrict the search to the specified job types as listed by the FlexJobs::Search.job_types method.
    #                 Defaults to temporary and freelance jobs.
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
    #   - logger - a ::Logger instance to log error messages to
    def initialize(options = {})
      @options = options
    end

    # Options include:
    #   - categories - restrict the search to the specified job categories as listed by the FlexJobs::Search.list_categories method.
    #   - career_level - restrict the search to the specified experience level as listed by the FlexJobs::Search.career_levels method.
    #   - work_schedule - restrict the search to the specified work schedule as listed by the FlexJobs::Search.work_schedules method.
    #   - job_types - restrict the search to the specified job types as listed by the FlexJobs::Search.job_types method.
    #                 Defaults to temporary and freelance jobs.
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
    #   - logger - a ::Logger instance to log error messages to
    #   - callback - an optional block to be called with each job found ASAP
    def job_search(keywords, options = {})
      configure_logger(options[:logger])

      search_url = URI.join(BASE_URL, SEARCH_PATH).to_s

      kws = CGI.escape(Array(keywords).join(" "))
      search_url << "?search=#{kws}"

      search_url << "&exclude=&location=&country=&tele_level=All+Telecommuting&will_travel=&accolade=&job_type=:job_type"

      categories = Array(options[:categories] || @options[:categories])
      category_ids_by_name = Search.send(:categories)
      categories.each do |category|
        category_id = category_ids_by_name[category]
        search_url << "&cats[]=#{category_id}" if category_id
      end

      career_level = options[:career_level] || @options[:career_level]
      career_level = (Search.career_levels & [career_level]).first.to_s
      search_url << "&career_level=#{CGI.escape(career_level)}"

      work_schedule = options[:work_schedule] || @options[:work_schedule]
      work_schedule = (Search.work_schedules & [work_schedule]).first.to_s
      search_url << "&schedule=#{CGI.escape(work_schedule)}"

      job_types = options[:job_types] || @options[:job_types] || Search.job_types
      job_types &= Search.job_types

      limit = options[:limit] || @options[:limit] || DEFAULT_LIMIT
      limit = [limit, DEFAULT_LIMIT].min

      max_page = options[:max_page] || @options[:max_page] || MAX_PAGE
      max_page = [max_page, MAX_PAGE].min

      jobs = []
      job_ids = Set.new
      current_page = 1

      catch(:done) do
        begin
          empty = job_types.map do |job_type|
            jobs_page_url = search_url.sub(":job_type", job_type)
            jobs_page_url << "&page=#{current_page}" if current_page > 1

            io = open_through_proxy(jobs_page_url)
            next(true) unless io

            jobs_page = Nokogiri::HTML(io)
            job_nodes = jobs_page.css("#joblist .list-group-item")
            if job_nodes.empty?
              true
            else
              job_nodes.each do |job_node|
                title_link = job_node.at_css("a")
                job_path = title_link["href"]
                id = job_path[/(\d+)\z/, 1]
                next if job_ids.include?(id)

                title = title_link.text
                job_url = URI.join(BASE_URL, job_path).to_s
                description = job_node.at_css(".job-description").text

                io = open_through_proxy(job_url)
                next unless io

                job_page = Nokogiri::HTML(io)
                details = job_page.at_css("table").css("tr").each_with_object({}) { |tr_node, hash|
                  detail_name = tr_node.at_css("th").text.chomp(":")
                  detail_value = tr_node.at_css("td").text
                  hash[detail_name] = detail_value
                }
                created_on = parse_date(details["Date Posted"])
                categories = details["Categories"].split(SEPARATED_BY_COMMAS_REGEXP)
                location = details["Location"].split(SEPARATED_BY_COMMAS_REGEXP)
                type = details["Job Type"].split(SEPARATED_BY_COMMAS_REGEXP)
                hours_per_week = details["Hours per Week"]
                career_level = details["Career level"]
                flexibility = details["Flexibility"].split(SEPARATED_BY_COMMAS_REGEXP)
                salary = details["Salary & Benefits"]
                other_benefits = details["Other Benefits"]
                benefits = other_benefits.split(SEPARATED_BY_COMMAS_REGEXP) if other_benefits

                job = Job.new(title, id, job_url, description, created_on, categories, location, type,
                              hours_per_week, career_level, flexibility, salary, benefits)
                yield(job) if block_given?
                jobs << job
                job_ids << id
                throw(:done) if jobs.size == limit
              end

              false
            end
          end

          done = empty.all?
          current_page += 1
          break if current_page > max_page
        end until done
      end

      jobs
    end

    private

    def configure_logger(logger = nil)
      @logger = logger || @options[:logger] || Logger.new(STDERR)
    end

    def parse_date(date)
      Date.strptime(date, DATE_FORMAT)
    end
  end
end
