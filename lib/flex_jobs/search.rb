require "nokogiri"
require_relative "proxies_helper"

module FlexJobs
  class Search
    SEARCH_PAGE_URL = "http://www.flexjobs.com/searchOptions.aspx"
    JOB_TYPES = %w(Temporary Freelance).freeze

    class << self
      include ProxiesHelper

      def list_categories
        @category_list || categories.keys
      end

      def career_levels
        @career_levels ||= search_page.css("#career_level option").map(&:text)[1..-1]
      end

      def work_schedules
        @work_schedules ||= search_page.at_css('select[name="schedule"]').css("option").map(&:text)[1...-1]
      end

      def job_types
        JOB_TYPES
      end

      private

      def logger
        @logger ||= Logger.new(STDERR)
      end

      def categories
        @categories ||= search_page.css("#cats option").each_with_object({}) { |option, hash|
          hash[option.text[/([A-Z].+)/, 1]] = option["value"]
        }
      end

      def search_page
        @search_page ||= Nokogiri::HTML(open_through_proxy(SEARCH_PAGE_URL))
      end
    end
  end
end
