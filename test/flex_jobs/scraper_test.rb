require File.expand_path("test/test_helper")
require File.expand_path("lib/flex_jobs/scraper")

module FlexJobs
  class ScraperTest < Minitest::Test
    def setup
      @flex_jobs = Scraper.new
      @keywords = "web developer"
    end

    def test_category_filtering
      jobs = @flex_jobs.job_search(@keywords, limit: 1, categories: "Web Design")
      assert_equal 1, jobs.size
    end

    def test_getting_three_freelance_jobs
      jobs = @flex_jobs.job_search(@keywords, limit: 3, job_type: "Freelance")
      assert_equal 3, jobs.size
    end

    def test_career_level_filtering
      jobs = @flex_jobs.job_search(@keywords, limit: 1, career_level: "Experienced (non-Manager)")
      assert_equal 1, jobs.size
    end

    def test_max_page_option
      jobs = @flex_jobs.job_search(@keywords, max_page: 1, limit: 100)
      assert_operator jobs.size, :<=, 50
    end

    def test_passing_in_a_callback_to_job_search
      job = nil
      @flex_jobs.job_search(@keywords, limit: 1) do |j|
        job = j
      end
      refute_nil job
    end
  end
end
