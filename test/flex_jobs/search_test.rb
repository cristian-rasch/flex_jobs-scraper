require_relative "../test_helper"
require File.expand_path("lib/flex_jobs/search")

module FlexJobs
  class SearchTest < Minitest::Test
    def test_categories
      refute_empty Search.list_categories
    end

    def test_career_levels
      refute_empty Search.career_levels
    end

    def test_work_schedules
      refute_empty Search.work_schedules
    end
  end
end
